//
//  ScanBodyPart.swift
//  AdhaHealth
//
//  Created by Najibullah Ulul Albab on 09/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct ScanBodyPart: View {
    var body: some View {
        ScrollView{
            VStack(alignment: .leading){
                Text("Mata")
                    .font(.title)
                HStack(spacing: 20){
                    BodyPart(imageName: "mata", text: "Kiri")
                    BodyPart(imageName: "mata", text: "Kanan")
                }
                Text("Kaki")
                    .font(.title)
                ScrollView(.horizontal){
                    HStack(spacing: 20){
                        BodyPart(imageName: "kaki", text: "Kiri Depan")
                        BodyPart(imageName: "kaki", text: "Kanan Depan")
                        BodyPart(imageName: "kaki", text: "Kiri Belakang")
                        BodyPart(imageName: "kaki", text: "Kanan Belakang")
                    }
                }
                
                Text("Tanduk")
                    .font(.title)
                HStack(spacing: 20){
                    BodyPart(imageName: "tanduk", text: "Kiri")
                    BodyPart(imageName: "tanduk", text: "Kanan")
                }
                Text("Mulut")
                    .font(.title)
                HStack(spacing: 20){
                    BodyPart(imageName: "gigi", text: "Kiri")
                    BodyPart(imageName: "gigi", text: "Kanan")
                }
                
            }
        .padding()
            
        }
    .navigationBarTitle("Pindai Bagian Tubuh")
        
    }
}

struct ScanBodyPart_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            ScanBodyPart()
        }
        
    }
}

struct BodyPart: View {
    var imageName: String = ""
    var text: String = ""
    var body: some View {
        ZStack{
            Rectangle()
                .fill(Color("Secondary"))
            VStack{
                Image(self.imageName)
                Text(self.text)
                    .foregroundColor(Color("Primary"))
            }
            
        }
        .frame(width: 125, height: 125)
        .cornerRadius(20)
        .padding(.vertical, 10)
    }
}
