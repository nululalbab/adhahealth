//
//  ContentView.swift
//  AdhaHealth
//
//  Created by Najibullah Ulul Albab on 09/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView{
            AnimalPick()
        }
        .navigationBarTitle("Pilih Hewan")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
       
            Group {
                ContentView()
                .environment(\.colorScheme, .dark)
                
                 ContentView()
                 .environment(\.colorScheme, .light)

               
            }

        
    }
}
