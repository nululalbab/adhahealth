//
//  AnimalPick.swift
//  AdhaHealth
//
//  Created by Najibullah Ulul Albab on 09/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct AnimalPick: View {
    var body: some View {
        ScrollView{
            Text("Cek kelayakan hewan Qurban")
                .font(.title)
            HStack{
                NavigationLink(destination: ScanBodyPart()){
                   AnimalPickCard(title: "Sapi", imageAnimal: "Sapi")
                }
                .buttonStyle(PlainButtonStyle())
                NavigationLink(destination: ScanBodyPart()){
                AnimalPickCard(title: "Kambing", imageAnimal: "Kambing")
                }
                 .buttonStyle(PlainButtonStyle())
            }
            HStack{
                Text("Pindai Terdahulu")
                    .font(.title)
                Spacer()
                Text("Lihat Semua")
                    .foregroundColor(Color.orange)
            }
            VStack{
                AnimalHistoryCard(imageName: "sapi-putih", name: "Sapi 004", date: "28 Juni 2020", health: "Sehat", akurasi: "90%")
                AnimalHistoryCard(imageName: "sapi-putih", name: "Sapi 004", date: "28 Juni 2020", health: "Sehat", akurasi: "90%")

                
            }
            
        }
    .navigationBarTitle("Pilih Hewan")
    .padding()
        
    }
}

struct AnimalPick_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            AnimalPick()
            
        }
              
        
    }
}

struct AnimalPickCard: View {
    var title: String = ""
    var imageAnimal: String = ""
    var body: some View {
        ZStack{
            Rectangle()
                .fill(Color("Primary"))
            .cornerRadius(20)
            VStack{
                Text(self.title)
                    .padding()
                    .foregroundColor(Color.black)
                Image(self.imageAnimal)
                    .frame(height: 210)
                    .scaledToFit()
                    .padding(0)
                ZStack(alignment: .topLeading){
                    Rectangle()
                        .fill(Color("Secondary"))
                    VStack(alignment: .leading){
                        Text("Bagian tubuh yang akan dipindai")
                            .foregroundColor(Color.orange)
                            .font(.body)
                            .fixedSize(horizontal: false, vertical: true)
                        .lineLimit(nil)
                          Spacer()
                        HStack{
                            Text("Mata")
                                .foregroundColor(Color.white)
                            Spacer()
                            Text("Kaki")
                                .foregroundColor(Color.white)
                        }
                        HStack{
                            Text("Gigi")
                                .foregroundColor(Color.white)
                            Spacer()
                            Text("Tanduk")
                                .foregroundColor(Color.white)
                        }
                    }
                    .padding()
                }
                
            }
        .cornerRadius(20)
        }
    }
}

struct AnimalHistoryCard: View {
    var imageName: String = ""
    var name: String = ""
    var date: String = ""
    var health: String = ""
    var akurasi: String = ""
    var body: some View {
        HStack{
            Image(self.imageName)
                .resizable()
                .frame(width: 75, height: 75)
                .aspectRatio(contentMode: .fit)
                .cornerRadius(15)
            VStack(alignment: .leading){
                HStack{
                    VStack{
                        Text(self.name)
                            .font(.body)
                        Text(self.date)
                            .font(.caption)
                    }
                    Spacer()
                    Image(systemName: "chevron.right")
                        .foregroundColor(Color.orange)
                }
                
                Spacer()
                HStack{
                    Text(self.health)
                        .foregroundColor(Color.green)
                    Spacer()
                    Text("Akurasi \(self.akurasi)")

                    .foregroundColor(Color.green)
                }
            }
        }
    }
}
